import numpy as np
import pandas as pd
from pandas_profiling import ProfileReport

file = pd.read_csv('2019_nCoV_data.csv', sep=';', index_col='Sno')

describe = file.describe()
print(describe)

ppr = ProfileReport(file, title='PPR', html={'style': {'full_width': True}})
ppr.to_file(output_file="3.html")



import matplotlib.pyplot as plt

plt.boxplot(file['Confirmed'])
plt.show()


