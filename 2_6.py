import numpy as np
import pandas as pd

df = pd.read_csv('mlbootcamp5_train.csv', sep=';',index_col='id')

bmi = ( df['weight'] / (( df['height'] / 100) ** 2) )
df.insert(loc=len(df.columns), column='bmi', value=bmi)
bmi_median = df['bmi'].median()
bmi_median_wooman = df[df['gender'] == 1]['bmi'].median()
bmi_median_man = df[df['gender'] == 2]['bmi'].median()
bmi_median_health = df[df['cardio'] == 0]['bmi'].median()
bmi_median_healthnt = df[df['cardio'] == 1]['bmi'].median()
bmi_median_health_alco = df[(df['alco'] == 0) & (['cardio'] == 0)]['bmi'].median()
bmi_median_health_alco_man = df[(df['alco'] == 0) & (df['cardio'] == 0) & (df['gender'] == 2)]['bmi'].median()
bmi_median_health_alco_wooman = df[(df['alco'] == 0) & (df['cardio'] == 0) & (df['gender'] == 1)]['bmi'].median()

print('#6')
print("{} - медіанний ВМІ перевищує норму".format(bmi_median) )
print("{} - медіанний ВМІ жінок".format(bmi_median_wooman))
print("{} - медіанний ВМІ чоловіків".format(bmi_median_man))
print("{} - медіанний ВМІ здорових".format(bmi_median_health))
print("{} - медіанний ВМІ хворих".format(bmi_median_healthnt))
print("{} - медіанний ВМІ здорових і вживаючих алкоголь чоловіків".format(bmi_median_health_alco_man))
print("{} - медіанний ВМІ здорових і вживаючих алкоголь жінок".format(bmi_median_health_alco_wooman))
