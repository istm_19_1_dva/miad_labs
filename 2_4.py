import numpy as np
import pandas as pd

df = pd.read_csv('mlbootcamp5_train.csv', sep=';',index_col='id')

smoking = df[df['smoke'] == 1]['age']
nosmoking = df[df['smoke'] == 0]['age']
print('#4')
print("На {} месяцев".format((nosmoking.median() - smoking.median())/30))
