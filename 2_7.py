import numpy as np
import pandas as pd

df = pd.read_csv('mlbootcamp5_train.csv', sep=';',index_col='id')

k = df[df['ap_hi']<df['ap_lo']]

w = df[(df['height'] > df['height'].quantile(.975)) | (df['height'] < df['height'].quantile(.025))]

z = df[(w['weight'] > df['weight'].quantile(.975)) | (df['weight'] < df['weight'].quantile(.025))]


perc = (df['gender'].count() / 100)

e = k['gender'].count() + w['gender'].count() + z['gender'].count();
print('#7')
print("Столько процентов данных мы выкинули - ", round(e/perc, 1),"%")

