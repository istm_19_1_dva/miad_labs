import numpy as np
import pandas as pd



df = pd.read_csv('500_Cities__City-level_Data__GIS_Friendly_Format___2019_release.csv', sep=',', index_col=0)



print('Pearson: ')
pearson = df.corr(method='pearson')
print(pearson)



print('Speerman: ')
spearman = df.corr(method='spearman')
print(spearman)