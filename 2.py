import numpy as np
import pandas as pd

df = pd.read_csv('mlbootcamp5_train.csv', sep=';',index_col='id')

data = df.groupby(['gender'])['height'].mean()
print('#1')
print("{} мужчин и {} женщин.".format(sum(df['gender'] == 2),
                                            sum(df['gender'] == 1)))
