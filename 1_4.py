import numpy as np
import pandas as pd

a = np.array([ [1, -3, 4], [-2,-1,2], [-6,7,-2] ])
b = np.array([ 9,5,1 ])

result = np.linalg.solve(a, b)

print(result)