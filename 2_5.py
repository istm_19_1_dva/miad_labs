import numpy as np
import pandas as pd

df = pd.read_csv('mlbootcamp5_train.csv', sep=';',index_col='id')

age_years = round(df['age'] / 365, 0)
df.insert(loc=len(df.columns), column='age_years', value=age_years)


old_man = df[(df['gender'] == 2) & (df['age_years'] > 60) & (df['age_years'] <= 64)]
old_man_smoke = old_man[old_man['smoke'] == 1]

firstGroup = old_man_smoke[(old_man_smoke['ap_hi'] < 120) & (old_man_smoke['cholesterol'] == 1)]
secondGroup = old_man_smoke[(old_man_smoke['ap_hi'] >= 160) & (old_man_smoke['ap_hi'] < 180)  & (old_man_smoke['cholesterol'] == 3)]


print('#5')
print("В {}, раз в первой группе больше чем во второй".format(sum(firstGroup['cardio'] == 1)/sum(secondGroup['cardio'] == 1)))
